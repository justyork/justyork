JUSTYORK Yii CMS
========

Starter pack for Yii2 website

##Installation
```
composer require justyork/justyork
```



What inside? 
-------------

#### Components
* [Seo](docs/components/Seo.md)

#### Models
* Pages
* News
* Block
* Seo
* Language

#### Widgets
* [Block](docs/widgets/Block.md)
* [ImageField](docs/widgets/ImageField.md)
* [LanguageForm](docs/widgets/LanguageForm.md)

### Behaviors
* [ImageField](docs/behaviors/ImageField.md)
* LanguageData

-----------
 
#### [Changelog]() 

 