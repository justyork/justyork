<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 29.09.2017
 * Time: 18:17
 */

namespace justyork\justyork\widgets;


use yii\helpers\Html;

use yii\base\Widget;
class Block extends Widget{


    public $name;
    public $clear = false;

    /* @var $model justyork\justyork\models\Block*/
    public $model;

    public function init(){
        $this->model = justyork\justyork\models\Block::find()->where(['title' => $this->name])->one();

    }

    public function run(){

        if(!$this->model){
            return Html::tag('div', Yii::t('app', 'Block "{block}" not found', ['block' => '<b>'.$this->name.'</b>']), ['style' => 'color: red; border: 1px solid red; padding: 5px 10px; display: inline-block;    background: #fce9e9; margin: 10px;']);
        }

        if($this->clear)
            return strip_tags($this->model->value);
        return $this->model->value;
    }
}