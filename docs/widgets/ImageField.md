ImageField widget
=============

[Go back](../../README.md)

---- 

## Configure 
Use it with [behavior/ImageField](../behaviors/ImageField.md)

## Using 

```php
<?=\justyork\justyork\widgets\ImageField::widget([
    'form' => $form,
    'model' => $model,
    'field' => 'imageFile' // Default: imageFile
])?>
```
 