LanguageForm widget
=============

[Go back](../../README.md)

---- 

Here is used the bootstrap [nav-tabs](https://getbootstrap.com/docs/4.0/components/navs/#tabs) module

## Using 
```php
<?=\justyork\justyork\widgets\LanguageForm::widget([
   "model" => $model,
   'columns' => [
       'title' => ['type' => 'textInput'],      // Text input
       'description' => ['type' => 'editor'],   // Text editor TinyMCE
       'notes' => ['type' => 'textarea'],       // Textarea
   ]
]);?>
```
 