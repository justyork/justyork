Block widget
=============

[Go back](../../README.md)

---- 

## Using 
```php
<?=\justyork\justyork\widgets\Block::widget([
    'name' => 'Phone',
    'clear' => true // Default: false (remove html tags)
]);?>
```
 