Seo Component
=============

[Go back](../../README.md)

----

## Configure

`/common/config/main.php`
````PHP
'components' => [
    'seo' => [
        'class' => 'justyork\justyork\components\Seo',
        'default_title' => 'My website',
        // 'default_description' => 'Description for my website',
        // 'default_keywords => 'my, awsome, web, site',
        // 'default_image => 'path/to/image.jpg',
    ],
    ...
],
'aliases' => [
    '@seo' => '/uploads/seo/', // Path to images
],
````

## Using


**Insert seo data** `/frontend/views/layouts/main.php`
```php
<head>
    ...
    <?=\Yii::$app->seo->head?>
    ...
</head>
```

For seo texts you can use
```php  
\Yii::$app->seo->top_text
```
and 
```php  
\Yii::$app->seo->bottom_text
```
