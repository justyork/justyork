Seo Component
=============

[Go back](../../README.md)

----

## Configure

In the model for which you want to download the file
````PHP
class Seo extends ActiveRecord{

    public $imageField; // tmp image file
    
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'justyork\justyork\behaviors\ImageField',
                'directory' => Yii::getAlias('@seo'),
                'field' => 'image', // Field in database
                'file' => 'imageFile',
            ], 
        ];
    }

    public function rules(){
        return [
            [['image'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }
    
}
````

## Using


Use [widget/ImageField](../widgets/ImageField.md) in your form